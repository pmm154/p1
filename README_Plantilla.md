# Backend CRUD API REST

_Ejemplo de WS REST con NodeJS que proporciona un API CRUD para gestionar una DB MongoDB._

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Ver **Deployment** para conocer cómo desplegar el proyecto.


### Pre-requisitos 📋

_Qué cosas necesitas para instalar el software y cómo instalarlas_

```
Para poder realizar esta práctica correctamente, es imprescindible instalar y ejecutar MongoDB. 

En esta práctica también serán necesarios varios programas utilizados en la guía 1 y 2.
Estos programas son los siguientes:
	
	1)Visual studio
			sudo snap install --classic code
	2)Instalación de NodeJS
			sudo apt update 
			sudo apt install npm
			sudo npm clean -f
			sudo npm i -g n
			sudo n stable
			
	3)Instalación del gestor de repositorios
			sudo install apt install git
		y configuramos git con nuestros datos de acceso

	4)Postman, lo utilizaremos para poder hacer invocaciones (HTTP request) a nuestro 		servidor como GET, PUT, DELETE etc.
			sudo snap install postman
				
	5)Gestor de repositorios
	También es importante instalar el gestor de proyectos Nodemon, el cual impide que tengamos que estaer constantemente reiniciando nuestra aplicación con cada cambio en el código fuente. Para 		su instalación, en primer lugar, abrimos una terminal y nos dirigimos a la carpeta en la que se encuetra nuestro proyecto y ejecutamos cualquiera de las 2 siguientesórdenes, ya que son 	equivalentes:
			npm i -D nodemon
			npm install -devDependencies nodemon
			
	6)Instalación de Express
	La instalación de esta biblioteca proporciona una capa adicional sobre NodeJS que facilita enormemente la gestión de métodos y recursos HTTP
			
			npm i -S express

```

### Instalación 🔧

_Una serie de ejemplos paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_

_Indica cómo será ese paso_

```
Como hemos dicho en el apartado anterior, es requisito indispensable MongoDB para esta práctica. Por tanto, deberemos instalarlo.

Para instalar Mongo, en el caso de trabajar en Ubuntu 20.04, debemos poner los siguientes comandos en el terminal:

			sudo apt update
			sudo apt install -y mongodb
			
Tras la instalación, es posible que se nos quede la base de datos lanzada. Si no es así, para iniciarla y gestionar su servicio utilizaremos el comando systemctl

	sudo systemctl start mongodb

En caso de querer verificar su funcionamiento, lo haremos de la siguiente forma:

			mongo --eval 'db.runCommand({ connectionStatus: 1 })'
			MongoDB shell version v3.6.8
			Connectoing to: mongodb://127.0.0.1:27017
			Implicit session: session { "id" : UUID(" 8f1128d1-4709-41bd-a64b-2bb618d3b08e ") }
			MondoDB server version: 3.6.8
			{
			"auhInfo" : {
			"authenticatedUsers" : [ ],
			"authenticatedUserRoles" : [ ]
			},
			"ok" : 1
			}

Finalmente, en otra terminal podemos abrir el gestor de la base de datos y probar directamente desde la terminal comandos para gestionarla

			mongo --host 127.0.0.1:27017
			show dbs


Finalmente, en nuestro proyecto tendremos que instalar la biblioteca mongodb para trabajar con la base de datos y, en nuestro caso, la biblioteca mongojs que nos simplifica el acceso a mongo sesde nuestro proyecto node.

Estas bibliotecas las instalaremos en el interior de nuestras carpetas node y api-rest
	
			cd node
			cd api-rest
			npm i -S mongodb
			npm i -S mongojs




```




_Finaliza con un ejemplo de cómo obtener datos del sistema o cómo usarlos para una pequeña demo_

## Ejecutando las pruebas ⚙️

_Explica cómo ejecutar las pruebas automatizadas para este sistema_


```
Una vez que tenemos todas las herramientas y bibliotecas, creamos nuestro servicio web CRUD con API RESTful
Para ello, debemos escribir código en nuestro archivo index.js
Este archivo se encuentra en nuestra carpeta api-rest, coontenida en nuestra carpeta node.
Para acceder a él desde el terminal debemos escribir los siguientes comandos:

			cd node
			cd api-rest
			code .

Una vez abierto, deberemos escribir el siguiente código:

			'use strict' 

			const port = process.env.PORT || 3000;

			const express = require('express');
			const logger = require('morgan');
			// utilizamos la biblioteca q nos proporciona mongojs
			const mongojs = require('mongojs');
			const app = express();
			// var db = mongojs('username:password@example.con/SD');
			var db = mongojs("SD");
			var id = mongojs.ObjectId;  //convierte un id en un objectoid
			app.use(logger('dev'));
			app.use(express.urlencoded({extended: false}));
			app.use(express.json());


			app.param("coleccion", (req, res, next, coleccion) => {
			    console.log('param /api/:coleccion');
			    console.log('colección: ', coleccion);
			    
			    req.collection = db.collection(coleccion);
			    return next();
			});


			app.get('/api', (req, res, next) => {
			    console.log('GET /api');
			    console.log(req.params);
			    console.log(req.collection);
			    
			    db.getCollectionNames((err, colecciones) => {
				if (err) return next(err);
				res.json(colecciones);
			    });
			});

			app.get('/api/:coleccion', (req, res, next) => {
			    req.collection.find((err, coleccion) => {
				if (err) return next(err);
				res.json(coleccion);
			    });
			});

			app.get('/api/:coleccion/:id', (req, res, next) => {
			    req.collection.findOne({_id: id(req.params.id)}, (err, elemento) => {
				if (err) return next(err);
				res.json(elemento);
			    });
			});

			app.post('/api/:coleccion', (req, res, next) => {
			    const elemento = req.body;
			   
			    if (!elemento.nombre) {
				res.status(400).json ({
				    error: 'Bad data',
				    description: 'Se precisa al menos un campo <nombre>'
				});
			    } else {
				req.collection.save(elemento, (err, coleccionGuardada) => {
				    if(err) return next(err);
				    res.json(coleccionGuardada);
				});
			    }
			});

			app.put('/api/:coleccion/:id', (req, res, next) => {
			    let elementoId = req.params.id;
			    let elementoData = req.body;

			    req.collection.update({_id: id(elementoId)}, 
				{$set: elementoData},{safe: true, multi: false}, (err, result) =>{
				
				if(err) return next(err);
				
				res.json(result);
			    });
			});

			app.delete('/api/:coleccion/:id', (req, res, next) => {
			    let elementoId = req.params.id;
			    
			    req.collection.remove({_id: id(elementoId)}, (err, resultado) => {
				if (err) return next(err);
				res.json(resultado);
			    });
			});


			app.listen(port, () => {
			    console.log(`API REST ejecutándose en http://localhost:${port}/api/:coleccion/:id`);
			});


Una vez tengamos nuestro servicio web creado, debemos recordar iniciar la base de datos mongoDB, en caso de no estarlo, abrimos 3 terminales y escribimos los siguientes comandos:


		--Terminal 1
			sudo systemctl start mongodb
		--Terminal 2
			cd node/api-rest
			npm start
		--Terminal 3
			mongo --host localhost:27017
			
Una vez escrito el código en nuuestro archivo index.js y iniciada la base de datos mongoDB, pocedemos a abrir POSTMAN y probar las diferentes rutas del API que ofrece nuestro servidor.			

			
```


### Analice las pruebas end-to-end 🔩

_Explica qué verifican estas pruebas y por qué_

```

1)Primera Peticion

	La primera prueba consiste en la creación de colecciones. Para ello utilizamos la llamada POST.
	
	Debemos poner en la cabecera (raw,text): Content-Type:application/json
	Cuerpo (raw, JSON)
		En el cuerpo deberemos encribir la informacion que queramos que aparezca. En el caso de estar creando la 			coleccion "familia", ponemos la información que corresponda a familia. En el caso de estar creando la 			colección "mascotas" escribimos la información que queramos sobre mascotas.
		
2)Segunda Peticion

				GET http://localhost:3000/api

	Realizamos una peición tipo GET, en la que llamamos a API, aquí nos apareceran el nombre de las colecciones 	creadas gracias al POST.
	
	(En el caso de mi practica, he creado 3 colecciones: "mascotas" "mascota" "familia". SIn querer he creado dos 		colecciones iguales y solo he introducido datos en la de familia, lo tengo que arreglar.)


3)Tercera Peticion

				GET http://localhost:3000/api/familia

	Al realizar esta petición nos aparecerá la información guardada en la colección familia

4)Cuarta Peticion

				GET http://localhost:3000/api/mascotas

	Al realizar esta petición nos aparecerá la información guardada en la colección mascotas
	
4) Quinta Peticion

				PUT http://localhost:3000/api/mascotas/id

	Al realizar esta petición se modificará/añade  en la colección determinada en la ruta el elemento de la coleccion al que le corresponda el id. 
	En el caso de mi practica añadimos 
	
		//Lo escribimos en el body.
		
				{
				"color"="marron"
				"nombre"="Luna"
				}
	Esta misma petición la podemos realizar también para la coleccion familia. Escribimos la modificacion en el body.
				PUT http://localhost:3000/api/familia/id
	
	
6) Sexta Petición

	Esta petición muestra por pantalla la  modificación realizada en la anterior petición.
				GET http://localhost:3000/api/mascotas    MUESTRA MODIFICACION REALIZADA EN mascotas
				GET http://localhost:3000/api/familia	   MUESTRA LA MODIFICACIÓN REALIZADA EN familia
		

```
### Y las pruebas de estilo de codificación ⌨️

_Explica qué verifican estas pruebas y por qué_

```
Para verificar toda la información que hemos creado se encuentra realmente en la base de datos utilizamos el cliente de mongoDB (llamado mongo).
Para ello podemos debemos escribir los siguiente comando en la terminal.

				mongo
				show dbs
				show collections
				db.familia.find()
				db.mascotas.find()
Una vez comprobado que toda la información se encuentra almacenada en nuestra base de datos, exportamos nuestra carpeta del postman y subimos todo lo que hemos realizado al repositorio remoto.

				git add .
				git commit -m "APIREST con MongoDB"
				git push
				git tab v3.0.0
				git push --tags


```
```
```
## Despliegue 📦

_Agrega notas adicionales sobre cómo hacer deploy_

## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - El framework web usado
* [Maven](https://maven.apache.org/) - Manejador de dependencias
* [ROME](https://rometools.github.io/rome/) - Usado para generar RSS

## Contribuyendo 🖇️

Por favor lee el [CONTRIBUTING.md](https://gist.github.com/tu/tuProyecto) para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra [Wiki](https://github.com/tu/proyecto/wiki)

## Versionado 📌

Usamos [SemVer](http://semver.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://github.com/tu/proyecto/tags).

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Paco Maciá** - *Trabajo Inicial* - [pmacia](https://github.com/pmacia)
* **Fulanito Detal** - *Documentación* - [fulanitodetal](#fulanito-de-tal)

También puedes mirar la lista de todos los [contribuyentes](https://github.com/your/project/contributors) quiénes han participado en este proyecto. 

## Licencia 📄

Este proyecto está bajo la Licencia (Tu Licencia) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 o un café ☕ a alguien del equipo. 
* Da las gracias públicamente 🤓.
* etc.
