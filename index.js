'use strict' 

const port = process.env.PORT || 3000;
const express = require('express');
const logger = require('morgan');
// utilizamos la biblioteca q nos proporciona mongojs
const mongojs = require('mongojs');
const cors = require('cors');
const app = express();

//GUÍA 5
const https= require('https');
const fs=require('fs');
const OPTIONS_HTTPS= {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};
// Imports

// Middlewares
var allowCrossTokenHeader = (req, res, next) => {
res.header("Access-Control-Allow-Headers", "*");
return next();
};
var allowCrossTokenOrigin = (req, res, next) => {
res.header("Access-Control-Allow-Origin", "*");
return next();
};
app.use(cors());
app.use(allowCrossTokenHeader);
app.use(allowCrossTokenOrigin);
// var db = mongojs('username:password@example.con/SD');
var db = mongojs("SD");
var id = mongojs.ObjectId;  //convierte un id en un objectoid
//AÑADIMOS EL CÓDGO DE LA GUIA4_P1 
//PARA RESOLVER EL PROBLEMA DE AJAX CON LA BIBLIOTECA CORS
// Declaraciones
var allowCrossTokenHeader = (req, res, next) => {
    res.header("Access-Control-Allow-Headers", "*");
    return next();
    };
    var allowCrossTokenOrigin = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    return next();
};
//TOKEN. AUTENTIFICACIÓN
var auth = (req, res, next) => {
   if(req.headers.token === "password1234") {
    return next();
    } else {
    return next(new Error("No autorizado"));
    };
};
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(cors()); //Biblioteca CORS, practica4_1
app.use(allowCrossTokenHeader); //p4_1
app.use(allowCrossTokenOrigin);

app.param("coleccion", (req, res, next, coleccion) => {
    console.log('param /api/:coleccion');
    console.log('colección: ', coleccion);
    
    req.collection = db.collection(coleccion);
    return next();
});
app.get('/api', (req, res, next) => {
    console.log('GET /api');
    console.log(req.params);
    console.log(req.collection);
    
    db.getCollectionNames((err, colecciones) => {
        if (err) return next(err);
        res.json(colecciones);
    });
});
app.get('/api/:coleccion', (req, res, next) => {
    req.collection.find((err, coleccion) => {
        if (err) return next(err);
        res.json(coleccion);
    });
});

app.get('/api/:coleccion/:id', (req, res, next) => {
    req.collection.findOne({_id: id(req.params.id)}, (err, elemento) => {
        if (err) return next(err);
        res.json(elemento);
    });
});
app.post('/api/:coleccion', (req, res, next) => {
    const elemento = req.body;
   
    if (!elemento.nombre) {
        res.status(400).json ({
            error: 'Bad data',
            description: 'Se precisa al menos un campo <nombre>'
        });
    } else {
        req.collection.save(elemento, (err, coleccionGuardada) => {
            if(err) return next(err);
            res.json(coleccionGuardada);
        });
    }
});
//añadimos auth
app.put('/api/:coleccion/:id',auth, (req, res, next) => {
    let elementoId = req.params.id;
    let elementoData = req.body;

    req.collection.update({_id: id(elementoId)}, 
        {$set: elementoData},{safe: true, multi: false}, (err, result) =>{
        if(err) return next(err);
        res.json(result);
    });
});
//añadir auth
app.delete('/api/:coleccion/:id', auth, (req, res, next) => {
    let elementoId = req.params.id;
    req.collection.remove({_id: id(elementoId)}, (err, resultado) => {
        if (err) return next(err);
        res.json(resultado);
    });
});

https.createServer(OPTIONS_HTTPS,app).listen(port, () => {
    console.log(`SEC WS API REST CRUD Ccon DB  ejecutándose en https://localhost:${port}/api/:coleccion/:id`);
});

/*
app.listen(port, () => {
    console.log(`API REST ejecutándose en http://localhost:${port}/api/:coleccion/:id`);
});
*/
